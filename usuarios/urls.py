from rest_framework import routers
from .views import UsersViewSet

router = routers.DefaultRouter()
router.register(r'usuarios', UsersViewSet, basename='usuarios')


urlpatterns = router.urls